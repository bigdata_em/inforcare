-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 12, 2019 at 11:21 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inforcare`
--

-- --------------------------------------------------------

--
-- Table structure for table `aftercare`
--

DROP TABLE IF EXISTS `aftercare`;
CREATE TABLE IF NOT EXISTS `aftercare` (
  `medid` int(10) NOT NULL,
  `pracid` int(10) NOT NULL,
  `procname` varchar(45) NOT NULL,
  `type` varchar(15) NOT NULL,
  PRIMARY KEY (`medid`,`pracid`,`procname`,`type`),
  KEY `doctorid6` (`pracid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aftercarenotes`
--

DROP TABLE IF EXISTS `aftercarenotes`;
CREATE TABLE IF NOT EXISTS `aftercarenotes` (
  `medid` int(10) NOT NULL,
  `note` varchar(45) NOT NULL,
  PRIMARY KEY (`medid`,`note`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

DROP TABLE IF EXISTS `doctor`;
CREATE TABLE IF NOT EXISTS `doctor` (
  `pracid` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `password` varchar(15) NOT NULL,
  `capabilities` varchar(60) DEFAULT NULL,
  `email` varchar(40) NOT NULL,
  `phoneNumber` varchar(13) NOT NULL,
  PRIMARY KEY (`pracid`),
  UNIQUE KEY `email` (`email`,`phoneNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doctorfacility`
--

DROP TABLE IF EXISTS `doctorfacility`;
CREATE TABLE IF NOT EXISTS `doctorfacility` (
  `pracid` int(10) NOT NULL,
  `facid` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`pracid`,`facid`),
  KEY `facid1` (`facid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `facility`
--

DROP TABLE IF EXISTS `facility`;
CREATE TABLE IF NOT EXISTS `facility` (
  `facid` int(10) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `streetaddress` varchar(50) NOT NULL,
  `postalcode` varchar(7) NOT NULL,
  `country` varchar(30) NOT NULL,
  `type` varchar(20) DEFAULT NULL,
  `district` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`facid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fake1`
--

DROP TABLE IF EXISTS `fake1`;
CREATE TABLE IF NOT EXISTS `fake1` (
  `fakechar` varchar(20) NOT NULL,
  `myint` int(10) NOT NULL,
  PRIMARY KEY (`fakechar`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fake2`
--

DROP TABLE IF EXISTS `fake2`;
CREATE TABLE IF NOT EXISTS `fake2` (
  `fakechar` varchar(20) NOT NULL,
  `myint2` int(5) NOT NULL,
  PRIMARY KEY (`fakechar`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fake3`
--

DROP TABLE IF EXISTS `fake3`;
CREATE TABLE IF NOT EXISTS `fake3` (
  `fakechar` int(20) NOT NULL,
  `myint2` int(5) NOT NULL,
  PRIMARY KEY (`fakechar`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `guardian`
--

DROP TABLE IF EXISTS `guardian`;
CREATE TABLE IF NOT EXISTS `guardian` (
  `medid` int(10) NOT NULL,
  `relationship` varchar(20) DEFAULT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` int(15) NOT NULL,
  KEY `patientidguardian` (`medid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mentalhealth`
--

DROP TABLE IF EXISTS `mentalhealth`;
CREATE TABLE IF NOT EXISTS `mentalhealth` (
  `medid` int(10) NOT NULL,
  `type` varchar(30) NOT NULL,
  PRIMARY KEY (`medid`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
CREATE TABLE IF NOT EXISTS `patient` (
  `medid` int(10) NOT NULL,
  `name` varchar(30) NOT NULL,
  `password` varchar(15) NOT NULL,
  `email` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`medid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `patientdoctor`
--

DROP TABLE IF EXISTS `patientdoctor`;
CREATE TABLE IF NOT EXISTS `patientdoctor` (
  `medid` int(11) NOT NULL,
  `pracid` int(11) NOT NULL,
  `referral` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`medid`,`pracid`),
  KEY `doctorid` (`pracid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `patientdoctormessage`
--

DROP TABLE IF EXISTS `patientdoctormessage`;
CREATE TABLE IF NOT EXISTS `patientdoctormessage` (
  `medid` int(10) NOT NULL,
  `pracid` int(10) NOT NULL,
  `message` varchar(100) NOT NULL,
  `datetime` varchar(20) NOT NULL,
  PRIMARY KEY (`medid`,`pracid`,`datetime`),
  KEY `doctorid2` (`pracid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `patientfacility`
--

DROP TABLE IF EXISTS `patientfacility`;
CREATE TABLE IF NOT EXISTS `patientfacility` (
  `medid` int(10) NOT NULL,
  `facid` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`medid`,`facid`),
  KEY `facilityid` (`facid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `patientprocedure`
--

DROP TABLE IF EXISTS `patientprocedure`;
CREATE TABLE IF NOT EXISTS `patientprocedure` (
  `medid` int(10) NOT NULL,
  `pracid` int(10) NOT NULL,
  `procname` varchar(45) NOT NULL,
  PRIMARY KEY (`medid`,`pracid`,`procname`),
  KEY `doctorid7` (`pracid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `physiotherapy`
--

DROP TABLE IF EXISTS `physiotherapy`;
CREATE TABLE IF NOT EXISTS `physiotherapy` (
  `medid` int(10) NOT NULL,
  `type` varchar(20) NOT NULL,
  `schedule` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`medid`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `procedurerisks`
--

DROP TABLE IF EXISTS `procedurerisks`;
CREATE TABLE IF NOT EXISTS `procedurerisks` (
  `pracid` int(10) NOT NULL,
  `procname` varchar(45) NOT NULL,
  `facid` int(10) UNSIGNED NOT NULL,
  `risk` varchar(45) NOT NULL,
  PRIMARY KEY (`pracid`,`procname`,`facid`,`risk`),
  KEY `facilityid5` (`facid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `procedures`
--

DROP TABLE IF EXISTS `procedures`;
CREATE TABLE IF NOT EXISTS `procedures` (
  `pracid` int(10) NOT NULL,
  `procname` varchar(45) NOT NULL,
  `facid` int(10) UNSIGNED NOT NULL,
  `duration` varchar(45) NOT NULL,
  `docaid` varchar(45) NOT NULL,
  PRIMARY KEY (`pracid`,`procname`,`facid`),
  KEY `facilityid6` (`facid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `aftercare`
--
ALTER TABLE `aftercare`
  ADD CONSTRAINT `doctorid6` FOREIGN KEY (`pracid`) REFERENCES `doctor` (`pracid`),
  ADD CONSTRAINT `patientid1` FOREIGN KEY (`medid`) REFERENCES `patient` (`medid`);

--
-- Constraints for table `aftercarenotes`
--
ALTER TABLE `aftercarenotes`
  ADD CONSTRAINT `patientid2` FOREIGN KEY (`medid`) REFERENCES `patient` (`medid`);

--
-- Constraints for table `doctorfacility`
--
ALTER TABLE `doctorfacility`
  ADD CONSTRAINT `doctorid3` FOREIGN KEY (`pracid`) REFERENCES `doctor` (`pracid`),
  ADD CONSTRAINT `facid1` FOREIGN KEY (`facid`) REFERENCES `facility` (`facid`);

--
-- Constraints for table `fake2`
--
ALTER TABLE `fake2`
  ADD CONSTRAINT `sdfgsdfg` FOREIGN KEY (`fakechar`) REFERENCES `fake1` (`fakechar`);

--
-- Constraints for table `guardian`
--
ALTER TABLE `guardian`
  ADD CONSTRAINT `patientidguardian` FOREIGN KEY (`medid`) REFERENCES `patient` (`medid`);

--
-- Constraints for table `mentalhealth`
--
ALTER TABLE `mentalhealth`
  ADD CONSTRAINT `pateintid5` FOREIGN KEY (`medid`) REFERENCES `patient` (`medid`);

--
-- Constraints for table `patientdoctor`
--
ALTER TABLE `patientdoctor`
  ADD CONSTRAINT `doctorid` FOREIGN KEY (`pracid`) REFERENCES `doctor` (`pracid`),
  ADD CONSTRAINT `pateintid` FOREIGN KEY (`medid`) REFERENCES `patient` (`medid`);

--
-- Constraints for table `patientdoctormessage`
--
ALTER TABLE `patientdoctormessage`
  ADD CONSTRAINT `doctorid2` FOREIGN KEY (`pracid`) REFERENCES `doctor` (`pracid`),
  ADD CONSTRAINT `pateintid2` FOREIGN KEY (`medid`) REFERENCES `patient` (`medid`);

--
-- Constraints for table `patientfacility`
--
ALTER TABLE `patientfacility`
  ADD CONSTRAINT `facilityid` FOREIGN KEY (`facid`) REFERENCES `facility` (`facid`),
  ADD CONSTRAINT `patientid` FOREIGN KEY (`medid`) REFERENCES `patient` (`medid`);

--
-- Constraints for table `patientprocedure`
--
ALTER TABLE `patientprocedure`
  ADD CONSTRAINT `doctorid7` FOREIGN KEY (`pracid`) REFERENCES `doctor` (`pracid`),
  ADD CONSTRAINT `pateintid3` FOREIGN KEY (`medid`) REFERENCES `patient` (`medid`);

--
-- Constraints for table `physiotherapy`
--
ALTER TABLE `physiotherapy`
  ADD CONSTRAINT `pateintid4` FOREIGN KEY (`medid`) REFERENCES `patient` (`medid`);

--
-- Constraints for table `procedurerisks`
--
ALTER TABLE `procedurerisks`
  ADD CONSTRAINT `doctorid4` FOREIGN KEY (`pracid`) REFERENCES `doctor` (`pracid`),
  ADD CONSTRAINT `facilityid5` FOREIGN KEY (`facid`) REFERENCES `facility` (`facid`);

--
-- Constraints for table `procedures`
--
ALTER TABLE `procedures`
  ADD CONSTRAINT `doctorid5` FOREIGN KEY (`pracid`) REFERENCES `doctor` (`pracid`),
  ADD CONSTRAINT `facilityid6` FOREIGN KEY (`facid`) REFERENCES `facility` (`facid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
