<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpresstest' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'N*TA.eR|BDhRJ;,zZ{n^Xw(BxW0rt~|TGU.b,?4BYs7i`$yE&v;{JiJv(C)y]3 k' );
define( 'SECURE_AUTH_KEY',  '[JR.qFM;guH+`jB]OB*Pb3`N.C[Lrq3Jp|XLIP|KU}54NW*b,Tjd4F2+BA`o/=:K' );
define( 'LOGGED_IN_KEY',    'TzB(qi E&Uo:60~?rh=&P3)Oi+:=cT-nCP+P%Z(t2]$^Om2s74>7} CuD!?!y&bx' );
define( 'NONCE_KEY',        'F`SQyP+4D?YEL|l}X7@FAcBkR]{6c+ZV!#_8vZVV?3s.n6a]{O*cf)y#.WJ_+,%l' );
define( 'AUTH_SALT',        'nEf_;EBQn1m%7PC]]Aj`4LODG*|nbmH4gQrs:L tN3CU5_j>kUY<YsC-T3 ){Do$' );
define( 'SECURE_AUTH_SALT', 'Deew;K=dQmjy|T-o^!uIB^$)mL{6cTo3oiss]5Xmo#gN^,~u~qP,uZA~1^AYLW3q' );
define( 'LOGGED_IN_SALT',   'I(1&~7|y>1(K7RX`Noeqn*>>,uCaOU2t&qa-?183P`e dc:HPgtat[vuPQS)F2O ' );
define( 'NONCE_SALT',       '1>yj&=zRwyHxD:dgmHcEJK;MIN4NNX14B2aCC-37}96K^_0w<Bt5N)RwQ_;C+e/e' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
